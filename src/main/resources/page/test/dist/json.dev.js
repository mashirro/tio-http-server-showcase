"use strict";

function submitForm() {
  var arr = {
    id: 111,
    shortid: 11,
    longid: 12345678,
    byteid: 1,
    floatid: 12345678.22,
    doubleid: 1234567890.22,
    loginname: 'talent',
    nick: 'tan',
    ip: '127.0.0.1'
  };
  $.ajax({
    url: '/showcase/json',
    type: 'POST',
    // data: JSON.stringify(arr),
    data: $("#user").val(),
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    async: false,
    success: function success(resp) {
      $("#msg").html(resp.msg);
    }
  });
}